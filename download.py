# -*- coding: utf-8 -*-


import os
import sys
import json
import time
import pycurl
import sqlite3
import logging
import urllib.parse
from io import BytesIO
from PIL import Image
from bs4 import BeautifulSoup
from os.path import join, getsize


def initCurl():
    try:
        with open("config.json", 'r') as config_file:
            config = json.load(config_file)
    except Exception as err:
        print("Open Config File Error!")
        print(err)
        logging.error(err)
        time.sleep(5)
        sys.exit()
    bUseProxy = config['UseProxy']
    proxyAddress = config['ProxyAddress']
    proxyUser = config['ProxyUser']
    proxyPasswd = config['ProxyPasswd']
    c = pycurl.Curl()
    c.setopt(pycurl.FOLLOWLOCATION, 1)
    c.setopt(pycurl.MAXREDIRS, 5)
    if bUseProxy:
        c.setopt(pycurl.PROXY, proxyAddress)
        if not proxyUser == None:
            c.setopt(pycurl.PROXYUSERPWD, proxyUser + ':' + proxyPasswd)
    return c


def getHtml(curl, url):
    head = ['Accept:*/*',
            'User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0']
    buf = BytesIO()
    curl.setopt(pycurl.WRITEFUNCTION, buf.write)
    curl.setopt(pycurl.URL, url)
    curl.setopt(pycurl.HTTPHEADER, head)
    curl.setopt(pycurl.SSL_VERIFYHOST, 0)
    curl.setopt(pycurl.SSL_VERIFYPEER, 0)
    curl.perform()
    page = buf.getvalue()
    buf.close()
    return page


def setLogging():
    try:
        with open("config.json", 'r') as config_file:
            config = json.load(config_file)
    except Exception as err:
        print("Open Config File Error!")
        print(err)
        logging.error(err)
        time.sleep(5)
        sys.exit()
    logfile = config['LogFile']
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S', filename=logfile, filemode='a')


def connectDatabase():
    db = sqlite3.connect("database.db")
    return db


def CheckDiskSpace():
    disk = os.statvfs('/')
    free = (disk.f_bavail * disk.f_frsize) / 1024 / 1024
    if free < 1024:
        return 1
    else:
        return 0


def getDiskSpace():
    disk = os.statvfs('/')
    free = (disk.f_bavail * disk.f_frsize) / 1024 / 1024
    return free


def getImageSize():
    size = 0
    for (root, dirs, files) in os.walk("/var/www/images/images/"):
        for name in files:
            try:
                size += getsize(join(root, name))
            except:
                continue
    size = size / 1024 / 1024
    return size


def getIndexUrl(rating, tag):
    index = 'https://gelbooru.com/index.php?page=post&s=list&tags=' + \
            tag + '+rating%3A' + rating
    with open("config.json", 'r') as config_file:
        config = json.load(config_file)
    blacklist = config['BlackList']
    for tag in blacklist:
        tag = urllib.parse.quote(tag)
        index = index + '+-' + tag
    height = "height:>=" + str(config['min_height'])
    height = urllib.parse.quote(height)
    width = "width:>=" + str(config['min_width'])
    width = urllib.parse.quote(width)
    index = index + '+' + width + '+' + height
    return index


def getPageCount(curl, index):
    html = getHtml(curl, index)
    soup = BeautifulSoup(html, 'lxml')
    max_page = 0
    try:
        pagination = soup.find('div', class_='pagination').select('a')
        if pagination:
            for page in pagination:
                page_num = page.string
                page_url = page.attrs['href']
                if page_num == '»':
                    pid = page_url[page_url.find('pid'):].replace('pid=', '')
                    max_page = int(pid) // 42 + 1
                    break
                if not page_num == '›':
                    max_page = int(page_num)
        else:
            pagination = soup.find('div', class_='pagination').select('b')
            if pagination:
                max_page = 1
            else:
                max_page = 0
    except Exception as err:
        print(err)
        logging.error(err)
        max_page = 0
    return max_page


def getListPage(curl, index, page):
    pid = 42 * page
    if page == 0:
        listpage = index
    else:
        listpage = index + '&pid=' + str(pid)
    html = getHtml(curl, listpage)
    return html


def isImageExist(img_id):
    db = connectDatabase()
    cursor = db.cursor()
    sql = """SELECT * FROM images WHERE serial = %s;""" % (img_id)
    cursor.execute(sql)
    results = cursor.fetchone()
    db.close()
    if results == None:
        return False
    else:
        return True


def CheckPath(path):
    if not os.path.exists(path):
        os.makedirs(path)


def getImage(curl, rating, tag, pageurl):
    image = {}
    image['tag'] = tag
    image['rating'] = rating
    html = getHtml(curl, pageurl)
    soup = BeautifulSoup(html, 'lxml')
    tags = soup.find("textarea").get_text()
    tags = tags.replace("\'", '')
    tags = tags.replace("\"", '')
    image['tags'] = tags
    refer = soup.find(attrs={'rel': 'nofollow'}).attrs['href']
    image['refer'] = refer
    for text in soup.select("a"):
        try:
            if "Original image" in text:
                if ("webm" not in text) and ("gif" not in text):
                    imageurl = text["href"]
                    image['url'] = imageurl
                    data = getHtml(curl, imageurl)
                    filename = imageurl[imageurl.rfind('/') + 1:]
                    image['file'] = filename
                    with open("config.json", 'r') as config_file:
                        config = json.load(config_file)
                    basepath = config['BasePath']
                    path = basepath + 'images/' + rating + "/" + tag + '/'
                    CheckPath(path)
                    path = path + filename
                    with open(path, "wb") as img:
                        img.write(data)
                    img = Image.open(path)
                    image['width'] = img.size[0]
                    image['height'] = img.size[1]
                    # img.thumbnail((480, 360), Image.ANTIALIAS)
                    # path = basepath + 'thumbnail/' + rating + "/" + tag + '/'
                    # CheckPath(path)
                    # path = path + filename
                    # img.save(path)
                    return image
                else:
                    break
        except Exception as err:
            print(err)
            logging.error(err)
    return None


def updateTagsCount(tags):
    db = connectDatabase()
    cursor = db.cursor()
    taglist = tags.split(" ")
    for tag in taglist:
        sql = """SELECT * FROM tags WHERE tag = '%s';""" % (
            tag)
        cursor.execute(sql)
        results = cursor.fetchone()
        if results == None:
            sql = """INSERT INTO tags(tag, count) VALUES ('%s', 1);""" % (
                tag)
            try:
                cursor.execute(sql)
                db.commit()
            except Exception as err:
                print(err)
                logging.error(err)
                db.rollback()
        else:
            sql = """UPDATE tags SET count = count + 1 WHERE tag = '%s';""" % (
                tag)
            try:
                cursor.execute(sql)
                db.commit()
            except Exception as err:
                print(err)
                logging.error(err)
                db.rollback()
    db.close()


def saveImageInfo(id, info):
    db = connectDatabase()
    cursor = db.cursor()
    sql = """INSERT INTO images(serial, tag, file, width, height, refer, tags, favor, rating) VALUES (%s, '%s', '%s', %s, %s, '%s', '%s', 0, '%s');""" % (
        id, info['tag'], info['file'], info['width'], info['height'], info['refer'], info['tags'], info['rating'])
    try:
        cursor.execute(sql)
        db.commit()
    except Exception as err:
        print(err)
        logging.error(err)
        db.rollback()
    db.close()


def createDatabase():
    conn = sqlite3.connect('database.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE `images` (
        `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        `serial` INTEGER NOT NULL,
        `tag` TEXT NOT NULL,
        `file` TEXT NOT NULL,
        `width` INTEGER NOT NULL,
        `height` INTEGER NOT NULL,
        `refer` TEXT NOT NULL,
        `tags` TEXT NOT NULL,
        `favor` INTEGER NOT NULL,
        `rating` TEXT NOT NULL
        );''')
    conn.commit()
    c.execute('''CREATE TABLE `tags` (
        `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        `tag` TEXT NOT NULL,
        `count` INTEGER NOT NULL
        );''')
    conn.commit()
    conn.close()


setLogging()
if not os.path.exists('database.db'):
    try:
        createDatabase()
    except Exception as err:
        print("Create Database Error!")
        print(err)
        logging.error(err)
        time.sleep(5)
        sys.exit()
curl = initCurl()
try:
    with open("config.json", 'r') as config_file:
        config = json.load(config_file)
except Exception as err:
    print("Open Config File Error!")
    print(err)
    logging.error(err)
    time.sleep(5)
    sys.exit()
taglist = config['TagList']
ratinglist = config['RatingList']
for tag in taglist:
    for rate in ratinglist:
        t0 = time.time()
        print("Downloading Tag:%s Rating:%s" % (tag, rate))
        index = getIndexUrl(rate, tag)
        max_page = getPageCount(curl, index)
        total_counter = 0
        for i in range(0, max_page):
            try:
                html = getListPage(curl, index, i)
                page_counter = 0
                soup = BeautifulSoup(html, 'lxml')
                for pics in soup.find_all(attrs={'class': 'thumbnail-preview'}):
                    try:
                        detial_url = "http://gelbooru.com/" + pics.a["href"]
                        img_id = detial_url.replace(
                            'http://gelbooru.com/index.php?page=post&s=view&id=', '')
                        if not isImageExist(img_id):
                            imageinfo = getImage(
                                curl, rate, tag, detial_url)
                            if not imageinfo == None:
                                saveImageInfo(img_id, imageinfo)
                                updateTagsCount(imageinfo['tags'])
                        total_counter = total_counter + 1
                        page_counter = page_counter + 1
                        t1 = time.time()
                        ctime = t1 - t0
                        rtime = ctime / total_counter * max_page * 42 - ctime
                        cm, cs = divmod(ctime, 60)
                        ch, cm = divmod(cm, 60)
                        rm, rs = divmod(rtime, 60)
                        rh, rm = divmod(rm, 60)
                        print('Downloaded image %d on page %d.' %
                              (page_counter, i + 1))
                        logging.info(
                            'Downloaded image %d on page %d.' % (page_counter, i + 1))
                        print(
                            'Tag:%s Rating:%s Page: %d/%d  Image: %d/%d  Time: %02d:%02d:%02d Remaining: %02d:%02d:%02d' % (
                                tag, rate, i + 1, max_page, total_counter, max_page * 42, ch, cm, cs, rh, rm, rs))
                        logging.info(
                            'Tag:%s Rating:%s Page: %d/%d  Image: %d/%d  Time Elapsed: %02d:%02d:%02d Time Remaining: %02d:%02d:%02d' % (
                                tag, rate, i + 1, max_page, total_counter, max_page * 42, ch, cm, cs, rh, rm, rs))
                        while (CheckDiskSpace()):
                            time.sleep(30)
                            print("There is not enough disk space.")
                    except Exception as err:
                        print(err)
                        logging.error(err)
                        continue
            except Exception as err:
                print(err)
                logging.error(err)
                continue
    size = getImageSize()
    fsize = getDiskSpace()
    print("Images with tag %s download finished.    Size:%sMB   DiskSpace:%sMB" % (tag, size, fsize))
    if size > fsize - 1024:
        print("There is not enough disk space for zip.")
    else:
        os.system("zip -r -9 ./download/%s.zip ./images" % (tag))
        os.system("rm -rf ./images")
print('Download Finished!')
